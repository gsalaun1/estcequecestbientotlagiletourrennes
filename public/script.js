function compte_a_rebours(expectedDate, eventName, endDate) {

    const soon = document.getElementById("soon");

    const compte_a_rebours = document.getElementById("compte_a_rebours");

    const date_actuelle = new Date();

    const date_evenement = new Date(expectedDate);

    if(endDate !== undefined){
        const date_fin = new Date(endDate)
        if(date_actuelle > date_fin) {
            soon.innerHTML = "C'est déjà fini ! :("
            compte_a_rebours.innerHTML = "On se retrouve très bientôt pour la prochaine édition !"
            return
        }
    }

    const secondes_restantes = (date_evenement - date_actuelle) / 1000;

    if (secondes_restantes < 0) {
        soon.innerHTML = "OUI"
        compte_a_rebours.innerHTML = eventName + " a commencé !"
    }

    if (secondes_restantes > 0) {

        const jours = Math.floor(secondes_restantes / (60 * 60 * 24));

        const heures = Math.floor((secondes_restantes - (jours * 60 * 60 * 24)) / (60 * 60));

        const minutes = Math.floor((secondes_restantes - ((jours * 60 * 60 * 24 + heures * 60 * 60))) / 60);

        const secondes = Math.floor(secondes_restantes - ((jours * 60 * 60 * 24 + heures * 60 * 60 + minutes * 60)));

        if(jours < 7){
            soon.innerHTML = "Oui !"
        } else if(jours < 30) {
            soon.innerHTML = "Presque !"
        } else {
            soon.innerHTML = "Pas encore"
        }

        compte_a_rebours.innerHTML = `${eventName} démarre dans ${jours} jour${jours > 1 ? "s":""} ${heures} heure${heures > 1 ? "s":""} ${minutes} minute${minutes > 1 ? "s":""} ${secondes} seconde${secondes > 1 ? "s":""}`;

    }
}